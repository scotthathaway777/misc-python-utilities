cost = 250.00
taxRate = 8.00

def calcTax(taxRate, cost):
    return cost + ((taxRate / 100.00) * cost)

def formatCurrency(n):
    return "${:0,.2f}".format(float(n))

amt = calcTax(taxRate, cost)
print(f"The cost of the {formatCurrency(cost)} item with tax is:  {formatCurrency(amt)}")
