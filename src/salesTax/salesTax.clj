(defn calc-tax [tax-rate cost] 
    (let [amt + cost (* (/ tax-rate 100.00) cost)]
    (println "The cost of the " cost " item with tax is:  " amt)
    amt))
 
(calc-tax 8.00 250.00)
