# Profit Increase Calulator

import click

@click.command()
@click.option('--ii', default=600.00, help='Initial Investment Amount.')
@click.option('--pp', default=4.90, help='Profit Percentage.')
@click.option('--nd', default=366, help='Number of Days to Run.')
@click.option('--dta', default=1, help='Days to Accumulate.')

def calc(ii, pp, nd, dta):
    runningTotal = ii
    answer = 0
    for i in range(1, nd):
        answer = (i % dta)
        if answer == 0:
            runningTotal = runningTotal + (runningTotal * (pp / 100.00))
            print(f"Day {i} = {formatCurrency(runningTotal)}")
        else:
            if i == 1:
                print(f"Day {i} = {formatCurrency(ii)}")
    totalProfit = runningTotal - ii 
    totalProfitPercent = runningTotal / ii * 100
    print(f"Total Profit for {nd-1} Days: {formatCurrency(totalProfit)}  ({format2Decimals(totalProfitPercent)} %)")
    return True

def formatCurrency(n):
    return "${:0,.2f}".format(float(n))

def format2Decimals(n):
    return "{:0,.2f}".format(float(n))

calc()