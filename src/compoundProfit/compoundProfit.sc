// Profit Increase Calculator

def calc (profitPercent: Float, runningTotal: Float) =  
    runningTotal + (runningTotal * profitPercent / 100)

def calcItDone (startingDay: Integer, profitPercent: Float, runningTotal: Float): Float = 
    val amt = calc(profitPercent, runningTotal)
    println(s"Day $startingDay = $amt")
    return amt

def calcIt (startingDay: Integer, numDays: Integer, profitPercent: Float, runningTotal: Float): Float = 
    println(s"Day $startingDay = $runningTotal")
    if(startingDay == numDays) { 
        calcItDone((startingDay+1), profitPercent, runningTotal)
    } else {
        calcIt((startingDay+1), numDays, profitPercent, (calc(profitPercent, runningTotal)))
    }

@main def runner = 
    print(calcIt(0, 29, 6.55, 750.00))