defmodule CompoundProfit do

  def calc(profitPercent, runningTotal) do
    runningTotal + (runningTotal * profitPercent / 100)
  end

  def calcIt(startingDay, profitPercent, runningTotal) do
    IO.puts "Day #{startingDay} = #{runningTotal}"
    CompoundProfit.calc(profitPercent, runningTotal)
  end

  def calcIt(startingDay, numDays, profitPercent, runningTotal) do
    if startingDay == numDays do
      CompoundProfit.calcIt((startingDay+1), profitPercent, runningTotal)
    else
      IO.puts "Day #{startingDay} = #{runningTotal}"
      CompoundProfit.calcIt((startingDay+1), numDays, profitPercent, (calc(profitPercent,runningTotal)))
    end
  end

end

finalProfit = CompoundProfit.calcIt(0, 29, 6.55, 750.00)
IO.puts "Day 31 = #{finalProfit}"
