// Profit Increase Calculator

let calc profitPercent runningTotal =  
    let amt: float = runningTotal + (runningTotal * profitPercent / 100.00)
    amt

let calcItDone startingDay profitPercent runningTotal = 
    let amt = calc profitPercent runningTotal
    printfn "Day %i = %f" startingDay amt
    amt

let rec calcIt startingDay numDays profitPercent runningTotal = 
    printfn "Day %i = %f" startingDay runningTotal
    if int startingDay = int numDays then 
        calcItDone (startingDay+1) profitPercent runningTotal
    else
        calcIt (startingDay+1) numDays profitPercent (calc profitPercent runningTotal)


calcIt 0 29 6.55 750.00
