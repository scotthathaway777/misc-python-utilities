(defn ci-calc [profit-percent running-total]
  (let [pp-calc (/ profit-percent 100.00)
        inner-total-calc (* running-total pp-calc)
        total-calc (+ running-total inner-total-calc)]
   total-calc))

(defn run-ci
  ([profit-percent running-total]
   (let [new-total (ci-calc profit-percent running-total)]
    (println "Day 30: " new-total) 
    new-total))
   
  ([starting-day num-days profit-percent running-total]
   (println "Day" starting-day ":" running-total)
   (if (>= starting-day num-days) 
     (run-ci profit-percent running-total) 
     (run-ci (+ starting-day 1) num-days profit-percent (ci-calc profit-percent running-total)))))
  

(run-ci 0 29 6.55 750.0)
