-- Profit Increase Calculator

calc profitPercent runningTotal = do 
    runningTotal + (runningTotal * profitPercent / 100)

calcIt 0 0 profitPercent runningTotal = calc profitPercent runningTotal

calcIt startingDay numDays profitPercent runningTotal = do
    if startingDay == numDays 
    then calcIt 0 0 profitPercent runningTotal 
    else calcIt (startingDay+1) numDays profitPercent (calc profitPercent runningTotal)

main :: IO()
main = do 
    print (calcIt 1 30 6.55 750.00)
