import math 

def isPrime(number):
    sqrtNum = math.sqrt(number)
    for i in range(2, int(sqrtNum)+1):
        if (number / i).is_integer():
            return False 
    return True 

print(f"isPrime(10,000,000) = {isPrime(10_000_000)}")
print(f"isPrime(10,000,019) = {isPrime(10_000_019)}")
print(f"isPrime(10,000,000,027) = {isPrime(10_000_000_027)}")
print(f"isPrime(10,000,000,000,000) = {isPrime(10_000_000_000_000)}")
print(f"isPrime(10,000,000,000,000,000,000) = {isPrime(10_000_000_000_000_000_000)}")
